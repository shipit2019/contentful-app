import path from 'path';

export default function routes(app, addon) {
    // Redirect root path to /atlassian-connect.json,
    // which will be served by atlassian-connect-express.
    app.get('/', (req, res) => {
        res.redirect('/atlassian-connect.json');
    });

    // Verify that the incoming request is authenticated with Atlassian Connect.
    app.get('/app', addon.authenticate(), (req, res) => {
      res.sendFile(path.join(__dirname, '../dist/index.html'))
    });

  app.get('/auth', (req, res) => {
      res.sendFile(path.join(__dirname, '../dist/index.html'))
  });
}
