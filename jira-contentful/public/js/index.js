import React from 'react';
import {render} from 'react-dom';

function createSdk(token) {
  return (ids, spaceId = 'lo1gn5j3z3z5') => window.fetch(
    `https://api.contentful.com/spaces/${spaceId}/entries?sys.id[in]=${ids.join(',')}`,
    {
      headers: {
        'Authorization': `Bearer ${token}`,
      },
    }
  ).then(res => {
    if (res.ok) {
      return res.json();
    }

    console.log(res);
  });
}


function getItemStatus(item) {
  if (!!item.sys.archivedVersion) {
    return 'archived';
  }
  
  if (!item.sys.publishedVersion) {
    return 'draft';
  }

  if (!!(item.sys.publishedVersion && item.sys.version > item.sys.publishedVersion + 1)) {
    return 'changed';
  }

  return 'published';
}

function parseContentfulLink(link) {
  return link.value.records.map(v => {
    const [,,spaceId, environmentId, recordType, recordId] = v.split(':');
    return {
      spaceId,
      environmentId,
      recordType,
      recordId,
    };
  });
}

function getParsedContentfulLinks(issueKey) {
  return getContentfulLinks(issueKey)
    .then(parsed => parseContentfulLink(parsed));
}

function getContentfulLinks(issueKey) {
  return window.AP.request({
    url: `/rest/api/2/issue/${issueKey}/properties/contentfulLink`
  })
    .then(data => JSON.parse(data.body));
}

function addContentfulLink(issueKey, recordId) {
  return getContentfulLinks(issueKey).then(data => {
    const {records} = data.value;

    records.push(`ctf:entity:lo1gn5j3z3z5:master:Entry:${recordId}`);

    return window.AP.request({
        url: `/rest/api/2/issue/${issueKey}/properties/contentfulLink`,
        type: 'PUT',
        data: JSON.stringify({records}),
        contentType: 'application/json'
      });
  })
}

function removeRecordFromContentfulLink(issueKey, recordId) {
  return getContentfulLinks(issueKey)
    .then(data => {
      const {records} = data.value;
      return records.filter(link => !link.includes(recordId))
    })
    .then(filtered => {
      return window.AP.request({
          url: `/rest/api/2/issue/${issueKey}/properties/contentfulLink`,
          type: 'PUT',
          data: JSON.stringify({records: filtered}),
          contentType: 'application/json'
      });
    });
}

class App extends React.Component {
  constructor(props) {
    super(props);

    this.auth = this.auth.bind(this);
    this.getEntries = this.getEntries.bind(this);
    this.openInContentful = this.openInContentful.bind(this);
    this.unlinkEntry = this.unlinkEntry.bind(this);
    this.updateNewEntry = this.updateNewEntry.bind(this);
    this.addRecord = this.addRecord.bind(this);

    const token = localStorage.getItem('token');

    this.state = {
      loading: true,
      token,
      entries: [],
      sdk: token ? createSdk(token) : null,
      newEntry: '',
    };
  }

  componentDidMount() {
    if (window.location.pathname.includes('auth')) {
      const {hash} = window.location;
      const token = hash.split('&')[0].split('=')[1];

      localStorage.setItem('token', token);
      
      window.close();
    }

    if (!this.state.token) {
      const checker = setInterval(() => {
        const token = localStorage.getItem('token');

        if (token) {
          this.setState({token, sdk: createSdk(token)});
          this.getEntries();
          clearInterval(checker);
        }
      }, 500);
    } else {
      setInterval(() => {
        this.getEntries();
      }, 5000);
    }
  }

  auth() {
    const authUrl = [
      'https://be.contentful.com/oauth/authorize?',
      'response_type=token&',
      'client_id=YCareiH3uTPbPHRGjgIef4omrO_PehS00hOJjGZJHA4&',
      `redirect_uri=${window.location.origin}/auth&`,
      'scope=content_management_manage'
    ].join('');

    window.open(authUrl, 'jira', 'height=500,width=500,top=200,left=200');
  }

  getEntries() {
    if (!this.state.sdk) {
      return null;
    }

    window.AP.context.getContext().then(data => {
      return data.jira.issue.key;
    }).then(issueKey => {
      this.setState({issueKey});
      return getParsedContentfulLinks(issueKey);
    }).then(links => {
      const ids = links.map(link => link.recordId);
      return this.state.sdk(ids);
    }).then(entries => {
        this.setState({entries: entries.items, loading: false});
        const multi = entries.items.length > 1;
        let data;

        if (multi) {
          data = {
            type: 'badge',
            value: {
              label: entries.items.length,
            }
          };
        } else {
          let status = 'link entries';
          let statusType = 'default';

          if (entries.items.length === 0) {
            statusType = 'new';
          } else {
            status = getItemStatus(entries.items[0]);
          }


          if (status === 'published') {
            statusType = 'success';
          }

          if (status === 'archived') {
            statusType = 'moved';
          }

          if (status === 'changed') {
            statusType = 'inprogress';
          }

          data = {
            type: 'lozenge',
            value: {
              label: status,
              type: statusType,
            }
          };
        }
      
        window.AP.request({
          url: `/rest/api/2/issue/${this.state.issueKey}/properties/com.atlassian.jira.issue:contentful:contentful-issue-glances:status`,
          type: 'PUT',
          data: JSON.stringify(data),
          contentType: 'application/json'
        });
    });
  }

  openInContentful(id) {
    window.open(`https://app.contentful.com/spaces/lo1gn5j3z3z5/entries/${id}`)
  }

  unlinkEntry(id) {
    // optimistic removal
    this.setState(
      prevState => ({entries: prevState.entries.filter(e => e.sys.id !== id)})
    );

    removeRecordFromContentfulLink(this.state.issueKey, id)
      .then(() => this.getEntries());
  }

  updateNewEntry(e) {
    this.setState({newEntry: e.target.value});
  }

  addRecord() {
    const {newEntry} = this.state;
    if (!newEntry) {
      return;
    }

    if (!newEntry.includes('app.contentful.com')) {
      return;
    }

    const entryId = newEntry.split('/')[6];

    if (!entryId) {
      return;
    }

    addContentfulLink(this.state.issueKey, entryId).then(() => {
      this.setState({newEntry: ''});
      this.getEntries();
    });
  }

  render() {
    if (!this.state.token) {
      return (
        <div>
          <button onClick={this.auth}>Authenticate</button>
        </div>
      );
    }

    if (this.state.loading) {
      return (
        <div>loading</div>
      );
    }

    return (
      <div>
        <ul>
          {
            this.state.entries.map(item => {
              const status = getItemStatus(item);
              return (
              <li
                className="item"
              >
                <div
                  className="title"
                  onClick={() => this.openInContentful(item.sys.id)}
                >
                  {item.fields.title['en-US']}
                </div>
                <div className="pill-container">
                  <div className={`pill ${status}`}>
                    <span>{status}</span>
                  </div>
                  <div
                    className="ex"
                    title="Unlink entry"
                    onClick={() => this.unlinkEntry(item.sys.id)}
                  >
                    +
                </div>
                </div>
              </li>
              );
            })
          }
        </ul>
        <div className="add">
            {!this.state.entries.length && <div className="empty-text">You don't have any linked entries.</div>}
            <input value={this.state.newEntry} onChange={this.updateNewEntry} />
            <br />
          <button onClick={this.addRecord}>Link an entry</button>
        </div>
      </div>
    );
  }
}

render(<App/>, document.getElementById('root'));
