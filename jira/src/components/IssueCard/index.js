import React from "react";

import {
  Button,
  Card,
  Heading,
  Tooltip,
  Tag,
  Paragraph,
  CardActions,
  DropdownList,
  DropdownListItem,
  TextLink
} from "@contentful/forma-36-react-components";

const statusColors = {
  "medium-gray": "primary",
  green: "positive",
  yellow: "warning",
  brown: "secondary",
  "warm-red": "negative",
  "blue-gray": "muted"
};

export default ({
  issuetype,
  issueKey,
  priority,
  assignee,
  status,
  summary,
  onClick,
  onRemoveClick
}) => {
  return (
    <Card padding="none" className="jira-ticket">
      <div className="primary">
        <img className="type" src={issuetype.iconUrl} alt={issuetype.name} />
        <TextLink onClick={onClick} className="summary">
          {summary}
        </TextLink>
      </div>
      <div className="meta">
        <img src={priority.iconUrl} alt={priority.name} height={22} />
        {assignee ? (
          <Tooltip place="bottom" content={assignee.displayName}>
            <img
              className="avatar"
              src={assignee.avatarUrls["24x24"]}
              alt={assignee.displayName}
              height={22}
            />
          </Tooltip>
        ) : null}
        <Tag tagType={statusColors[status.statusCategory.colorName]}>
          {status.name}
        </Tag>
      </div>

      <CardActions>
        <DropdownList>
          <DropdownListItem onClick={onClick}>Open in Jira</DropdownListItem>
          <DropdownListItem onClick={() => onRemoveClick(issueKey)}>
            Unlink
          </DropdownListItem>
        </DropdownList>
      </CardActions>
    </Card>
  );
};
