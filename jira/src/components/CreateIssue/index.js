import React, { useState } from "react";

import {
  Button,
  Card,
  Heading,
  Tooltip,
  Tag,
  Paragraph,
  TextField,
  ToggleButton
} from "@contentful/forma-36-react-components";

export default ({
  issuetype,
  priority,
  assignee,
  status,
  issueTypes,
  client,
  sdk,
  onIssueCreated
}) => {
  const [issueType, setIssueType] = useState(issueTypes[0].id);
  const [summary, setSummary] = useState("");

  const onSubmit = async e => {
    e.preventDefault();

    if (summary.trim().length < 3) {
      sdk.notifier.error("You must specify a summary of the issue.");
      return;
    }

    onIssueCreated(await client.createIssue(summary.trim(), issueType));
    setSummary("");
  };

  return (
    <div id="create-issue">
      <form onSubmit={onSubmit}>
        <div className="form-input-group">
          <TextField
            value={summary}
            onChange={e => setSummary(e.target.value)}
            textInputProps={{
              placeholder: "Issue summary"
            }}
          ></TextField>
          <Button type="submit">Create</Button>
        </div>
        {issueTypes.map(({ id, name, iconUrl }) => (
          <ToggleButton
            key={id}
            isActive={id === issueType}
            onClick={() => setIssueType(id)}
          >
            <img src={iconUrl} alt={name} />
            {name}
          </ToggleButton>
        ))}
      </form>
    </div>
  );
};
