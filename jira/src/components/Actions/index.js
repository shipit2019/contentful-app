import React from "react";

import { ToggleButton, Icon } from "@contentful/forma-36-react-components";

export default ({ setForm, form }) => (
  <div id="actions">
    <ToggleButton onClick={() => setForm(1)} isActive={form === 1}>
      <Icon icon="Link"></Icon>
    </ToggleButton>
    <ToggleButton onClick={() => setForm(2)} isActive={form === 2}>
      <Icon icon="Plus"></Icon>
    </ToggleButton>
  </div>
);
