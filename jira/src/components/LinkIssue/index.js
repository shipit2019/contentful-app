import React, { useState } from "react";

import {
  Button,
  Card,
  Heading,
  Tooltip,
  Tag,
  TextField,
  DropdownList,
  DropdownListItem
} from "@contentful/forma-36-react-components";
import Autosuggest from "react-autosuggest";
import IssueCard from "../IssueCard";

export default ({ linkedIssues, client, sdk, onIssueLinked }) => {
  const [suggestions, setSuggestions] = useState([]);
  const [query, setQuery] = useState("");

  const onQueryChange = (event, { newValue }) => {
    setQuery(newValue);
  };

  const inputProps = {
    placeholder: "Search",
    value: query,
    onChange: onQueryChange
  };

  return (
    <div>
      <Autosuggest
        value={query}
        suggestions={suggestions}
        onSuggestionsFetchRequested={async ({ value }) => {
          const issues = await client.searchIssues(value);
          linkedIssues;

          const linkedIssuesKeys = linkedIssues.map(issue => issue.key);
          const filteredIssues = issues.filter(
            issue => !linkedIssuesKeys.includes(issue.key)
          );

          setSuggestions(filteredIssues);
        }}
        onSuggestionsClearRequested={() => setSuggestions([])}
        onSuggestionSelected={async (event, { suggestion }) => {
          onIssueLinked(suggestion);
        }}
        getSuggestionValue={suggestion => ""}
        renderSuggestion={props => (
          <DropdownListItem onClick={() => null}>
            <img
              className="type"
              src={props.issuetype.iconUrl}
              alt={props.issuetype.name}
            />
            {props.summary}
          </DropdownListItem>
        )}
        inputProps={inputProps}
        renderSuggestionsContainer={({ containerProps, children, query }) =>
          children ? (
            <DropdownList {...containerProps}>{children}</DropdownList>
          ) : null
        }
        renderInputComponent={inputProps => (
          <TextField
            textInputProps={{ placeholder: "Search for issues" }}
            {...inputProps}
          ></TextField>
        )}
      ></Autosuggest>
    </div>
  );
};
