const CLIENT_ID = "5sCkfGaQIqRVTbw4dtlWvsG13n9mYZ6J";
// UNSECURE (should be done with a backend service so the secret is hidden):
const CLIENT_SECRET =
  "h-pXyxvK0cwL6N_RyML4OlUveLGscddYT4gcYFU7IAjSAnaFI_lLqE_OM_k3gqr4";

const CONTENTFUL_LINK_PROPERTY_KEY = "contentfulLink";
const PROJECT_ID = "10000";

export class JiraClient {
  private projectId: string;
  private customFieldId: number;
  private jiraCloudId?: string = undefined;
  private jiraToken?: string = undefined;
  private jiraUrl?: string;

  constructor(sdk: any, customFieldId: number, projectId: string = PROJECT_ID) {
    this.customFieldId = customFieldId;
    this.projectId = projectId;
    this.sdk = sdk;
  }

  async loadToken() {
    const token = localStorage.getItem("jiraToken");
    if (token) {
      this.jiraToken = token;

      try {
        await this.getCloudId();
      } catch (e) {
        this.jiraToken = undefined;
      }
    }
  }

  storeToken() {
    if (this.jiraToken) {
      localStorage.setItem("jiraToken", this.jiraToken);
    }
  }

  async connectWithOauth(): Promise<void> {
    const codePromise: Promise<string> = new Promise(resolve => {
      window.addEventListener(
        "message",
        event => {
          const { data } = event;
          const { code } = data;

          if (!code) {
            return "";
          }

          resolve(code);
        },
        false
      );
    });

    window.open(
      `https://auth.atlassian.com/authorize?audience=api.atlassian.com&client_id=${CLIENT_ID}&scope=read%3Ajira-user%20read%3Ajira-work%20write%3Ajira-work&redirect_uri=${encodeURIComponent(
        window.location.href
      )}&state=test&response_type=code&prompt=consent`,
      "",
      "left=150,top=150,width=800,height=900"
    );

    const code = await codePromise;

    await this.initFromOauth(code);
    this.storeToken();
  }

  async initFromOauth(oauthCode: string) {
    await this.exchangeOauthCode(oauthCode);
    await this.getCloudId();
  }

  async initFromToken(token: string) {
    this.jiraToken = token;
    await this.getCloudId();
  }

  isConnected() {
    return !!this.jiraToken;
  }

  async exchangeOauthCode(oauthCode: string) {
    const response = await fetch("https://auth.atlassian.com/oauth/token", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        grant_type: "authorization_code",
        client_id: CLIENT_ID,
        client_secret: CLIENT_SECRET,
        code: oauthCode,
        redirect_uri: window.location.href
      })
    });

    const body = await response.json();
    this.jiraToken = body.access_token;
  }

  async getCloudId() {
    const response = await fetch(
      "https://api.atlassian.com/oauth/token/accessible-resources",
      {
        method: "GET",
        headers: {
          Authorization: "Bearer " + this.jiraToken
        }
      }
    );

    const resources = await response.json();
    this.jiraCloudId = resources[0].id;
    this.jiraUrl = resources[0].url;
  }

  getBaseUrl() {
    return `https://api.atlassian.com/ex/jira/${this.jiraCloudId}/rest/api/2`;
  }

  mapIssue = (jiraIssue: any) => {
    return {
      link: this.jiraUrl + "/browse/" + jiraIssue.key,
      key: jiraIssue.key,
      summary: jiraIssue.fields.summary,
      priority: jiraIssue.fields.priority,
      assignee: jiraIssue.fields.assignee,
      status: jiraIssue.fields.status,
      issuetype: jiraIssue.fields.issuetype
    };
  };

  getEntryUrn() {
    const { ids } = this.sdk;

    return `ctf:entity:${ids.space}:${ids.environment}:Entry:${ids.entry}`;
  }

  async getIssuesForEntryId(): Promise<any> {
    const jql = encodeURIComponent(
      `issue.property[contentfulLink].records = "${this.getEntryUrn()}"`
    );
    const result = await fetch(`${this.getBaseUrl()}/search?jql=${jql}`, {
      headers: {
        Authorization: `Bearer ${this.jiraToken}`
      }
    });

    const body = await result.json();
    return body.issues.map(this.mapIssue);
  }

  async searchIssues(text: string): Promise<any> {
    const jql = encodeURIComponent(
      `summary~"${text}" OR description~"${text}"`
    );
    const result = await fetch(`${this.getBaseUrl()}/search?jql=${jql}`, {
      headers: {
        Authorization: `Bearer ${this.jiraToken}`
      }
    });

    const body = await result.json();
    return body.issues.map(this.mapIssue);
  }

  async getContentfulLink(issueId: string) {
    const url = `${this.getBaseUrl()}/issue/${issueId}/properties/${CONTENTFUL_LINK_PROPERTY_KEY}`;

    const response = await fetch(url, {
      method: "GET",
      headers: {
        Authorization: `Bearer ${this.jiraToken}`,
        "Content-Type": "application/json",
        Accept: "application/json"
      }
    });

    if (response.status !== 200) {
      return { records: [] };
    }

    try {
      const body = await response.json();
      const linkData = body.value;

      if (!Array.isArray(linkData.records)) {
        linkData.records = [];
      }

      return linkData;
    } catch (err) {
      return {
        records: []
      };
    }
  }

  async createContentfulLink(issueId: string, link: any) {
    const url = `${this.getBaseUrl()}/issue/${issueId}/properties/${CONTENTFUL_LINK_PROPERTY_KEY}`;

    return await fetch(url, {
      method: "PUT",
      body: JSON.stringify(link),
      headers: {
        Authorization: `Bearer ${this.jiraToken}`,
        "Content-Type": "application/json",
        Accept: "application/json",
        "X-Atlassian-Token": "nocheck"
      }
    });
  }

  async addContentfulLink(issueId: string) {
    const existingLink = await this.getContentfulLink(issueId);
    existingLink.records.push(this.getEntryUrn());

    return this.createContentfulLink(issueId, existingLink);
  }

  async removeContentfulLink(issueId: string) {
    const existingLink = await this.getContentfulLink(issueId);
    existingLink.records = existingLink.records.filter(
      record => record !== this.getEntryUrn()
    );

    return this.createContentfulLink(issueId, existingLink);
  }

  async createIssue(summary: string, issueType: string) {
    const issue = {
      fields: {
        project: {
          id: this.projectId
        },
        summary,
        issuetype: {
          id: issueType
        }
      }
    };

    const url = `${this.getBaseUrl()}/issue`;

    const response = await fetch(url, {
      method: "POST",
      body: JSON.stringify(issue),
      headers: {
        Authorization: `Bearer ${this.jiraToken}`,
        "Content-Type": "application/json",
        Accept: "application/json",
        "X-Atlassian-Token": "nocheck"
      }
    });

    const createdIssue = await response.json();
    await this.createContentfulLink(createdIssue.id, {
      records: [this.getEntryUrn()]
    });

    return createdIssue;
  }

  async getProject() {
    const url = `${this.getBaseUrl()}/project/${
      this.projectId
    }?expand=issueTypes`;

    const response = await fetch(url, {
      method: "GET",
      headers: {
        Authorization: `Bearer ${this.jiraToken}`
      }
    });

    return response.json();
  }
}
