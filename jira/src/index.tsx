import * as React from "react";
import { render } from "react-dom";
import {
  Button,
  Card,
  Heading,
  Paragraph,
  SkeletonContainer,
  SkeletonBodyText,
  Note
} from "@contentful/forma-36-react-components";
import {
  init,
  locations,
  DialogExtensionSDK,
  SidebarExtensionSDK
} from "contentful-ui-extensions-sdk";
import tokens from "@contentful/forma-36-tokens";
import "@contentful/forma-36-react-components/dist/styles.css";
import "./index.css";
import { JiraClient } from "./jira-client";

import IssueCard from "./components/IssueCard";
import LinkIssue from "./components/LinkIssue";
import CreateIssue from "./components/CreateIssue";
import Actions from "./components/Actions";

import logo from "./logo.svg";

const PROJECT_ID = "10000";

export class SidebarExtension extends React.Component<{
  sdk: SidebarExtensionSDK;
}> {
  constructor(props) {
    super(props);

    this.state = {
      issues: [],
      project: null,
      isLoading: true,
      form: 0,
      client: new JiraClient(props.sdk, 10029, PROJECT_ID)
    };
  }
  async componentDidMount() {
    this.props.sdk.window.startAutoResizer();
    const { client } = this.state;

    await client.loadToken();
    if (client.isConnected()) {
      await this.loadIssues();
      this.setupPolling();
    }

    this.setState({ isLoading: false });
  }

  setupPolling = () => {
    setInterval(this.loadIssues, 5000);
  };

  loadIssues = async () => {
    const issues = await this.state.client.getIssuesForEntryId(
      this.props.sdk.ids.entry
    );

    issues.sort((a, b) => a.key.localeCompare(b.key));

    const project = await this.state.client.getProject();

    this.setState({
      issues,
      project
    });
  };

  onButtonClick = async () => {
    this.setState({ isLoading: true });
    await this.state.client.connectWithOauth();
    await this.loadIssues();
    this.setupPolling();
    this.setState({ isLoading: false });
  };

  onIssueLinked = async issue => {
    await this.state.client.addContentfulLink(issue.key);
    this.loadIssues();
  };

  onIssueCreated = issue => {
    this.loadIssues();
  };

  unlinkIssue = async issueId => {
    await this.state.client.removeContentfulLink(issueId);
    this.loadIssues();
  };

  renderForm = () => {
    const { form, project, issues, client } = this.state;
    const issueTypes = (project ? project.issueTypes : []).filter(
      i => i.subtask !== true
    );

    switch (form) {
      case 1:
        return (
          <LinkIssue
            linkedIssues={issues}
            client={client}
            sdk={this.props.sdk}
            onIssueLinked={i => this.onIssueLinked(i)}
          />
        );
      case 2:
        return (
          <CreateIssue
            issueTypes={issueTypes}
            sdk={this.props.sdk}
            client={client}
            onIssueCreated={i => this.onIssueCreated(i)}
          />
        );
      default:
        return null;
    }
  };

  setForm = form => {
    this.setState({ form });
  };

  render() {
    const { client, isLoading, issues, form } = this.state;

    if (isLoading) {
      return (
        <SkeletonContainer>
          <SkeletonBodyText numberOfLines={2} />
        </SkeletonContainer>
      );
    }

    return client && client.isConnected() ? (
      <div>
        <header>
          <img src={logo} alt="Jira Software" />
          <Actions setForm={this.setForm} form={form} />
        </header>

        {issues.length > 0 ? (
          issues.map(issue => (
            <IssueCard
              key={issue.key}
              onClick={() => window.open(issue.link, "_blank")}
              {...issue}
              issueKey={issue.key}
              onRemoveClick={i => this.unlinkIssue(i)}
            />
          ))
        ) : (
          <Note noteType={"primary"}>
            No issues are assigned to this entry.
          </Note>
        )}

        {this.renderForm()}
      </div>
    ) : (
      <Button
        testId="open-dialog"
        buttonType="positive"
        isFullWidth={true}
        onClick={this.onButtonClick}
      >
        Connect with Jira
      </Button>
    );
  }
}

init(sdk => {
  if (sdk.location.is(locations.LOCATION_DIALOG)) {
    render(
      <DialogExtension sdk={sdk as DialogExtensionSDK} />,
      document.getElementById("root")
    );
  } else {
    render(
      <SidebarExtension sdk={sdk as SidebarExtensionSDK} />,
      document.getElementById("root")
    );
  }
});

const searchParams = new URLSearchParams(location.search);

if (searchParams.get("code")) {
  window.opener.postMessage({ code: searchParams.get("code") }, "*");

  window.close();
}

/**
 * By default, iframe of the extension is fully reloaded on every save of a source file.
 * If you want to use HMR (hot module reload) instead of full reload, uncomment the following lines
 */
// if (module.hot) {
//   module.hot.accept();
// }
