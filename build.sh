#!/usr/bin/env bash

cd jira/
npm install
npm run build -- --no-inline
cd ..

cd jira-contentful
npm install
npm run start

cd ..
mkdir public/
cp -a jira/build/. public/contentful/
cp -a jira-contentful/dist public/app
cp -a jira-contentful/public/assets public/assets
cp jira-contentful/atlassian-connect.json public/atlassian-connect.json

